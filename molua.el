(require 'expand-region)
(require 'comint)
(require 'lua-mode)

(defvar lua--comint-buffer "*lua*")
(defvar lua--command "/usr/bin/lua")

(defun molua-hook ()
  (local-set-key (kbd "C-c C-c") 'molua-send-dwim)
  (local-set-key (kbd "C-c C-p") 'molua-start-comint)
  (local-set-key (kbd "C-M-x") 'lua-send-defun))

(add-hook 'lua-mode-hook 'molua-hook)

(defun molua-start-comint ()
  "Run lua command to start a comint"
  (interactive)
  (make-comint "lua" lua--command)
  (switch-to-buffer-other-window lua--comint-buffer))


(defun molua-send-dwim ()
  (interactive)
  (let ((caller-buffer (current-buffer)))
    (molua-start-comint)
    (while (not (eq (current-buffer) caller-buffer))
      (other-window 1)))
  (save-excursion
    (if (not (use-region-p))
        (lua-send-current-line)
      (lua-send-region
       (region-beginning)
       (region-end))))
  (deactivate-mark))

(provide 'molua)
